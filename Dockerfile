# Copyright 2020 N0r1sk
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

FROM ubuntu:24.04

RUN ln -snf /usr/share/zoneinfo/Europe/Vienna /etc/localtime \
  && apt-get update \
  && apt-get install -y ca-certificates gnutls-bin ldnsutils s-nail

COPY files/ /etc/dns-changer
COPY dns-changer /

RUN chmod u+x /dns-changer

CMD ["/dns-changer"]
