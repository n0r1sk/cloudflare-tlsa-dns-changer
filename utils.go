// Copyright 2020 N0r1sk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"bytes"
	"errors"
	"os"
	"os/exec"
	"reflect"
	"strconv"
	"time"

	log "github.com/sirupsen/logrus"
)

func hasElem(s interface{}, elem interface{}) bool {
	arrV := reflect.ValueOf(s)

	if arrV.Kind() == reflect.Slice {
		for i := 0; i < arrV.Len(); i++ {
			if arrV.Index(i).Interface() == elem {
				return true
			}
		}
	}

	return false
}

func executeSingleCommand(command string, arg ...string) (string, error) {
	cmd := exec.Command(command, arg...)
	var stdout, stderr bytes.Buffer
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr

	err := cmd.Run()

	if err != nil {
		return stdout.String(), errors.New(stderr.String())
	}

	return stdout.String(), nil
}

func stringSliceRemoveValue(s []string, r string) []string {
	for i, v := range s {
		if v == r {
			return append(s[:i], s[i+1:]...)
		}
	}
	return s
}

func parseToInt(value string) int {
	i, _ := strconv.Atoi(value)
	return i
}

func canSendEmail() bool {
	// Allow sending email twice per day
	if emailSentCount < 2 {
		if time.Since(lastEmailTime).Hours() >= 12 {
			emailSentCount = 0 // Reset the counter after 12 hours
		}
		return true
	}
	return false
}

func sendEmail(body string, domains string) {
	log.Infof("Send error mail for the following domains: %s", domains)
	out, err := executeSingleCommand("/bin/bash", "/etc/dns-changer/sendmail.sh", body, serviceConfig.MailNotification.From, domains, serviceConfig.MailNotification.SMTP, strconv.Itoa(serviceConfig.MailNotification.Port), serviceConfig.MailNotification.Password, serviceConfig.MailNotification.To)
	if err != nil {
		log.Warnf("Sending Email failed with the following error:\n%s\n%s", out, err)
	}
}

func setupLogging() {
	customFormatter := new(log.TextFormatter)
	customFormatter.TimestampFormat = "2006-01-02 15:04:05"
	customFormatter.FullTimestamp = true
	customFormatter.ForceColors = true
	log.SetFormatter(customFormatter)
	log.SetOutput(os.Stdout)
}
