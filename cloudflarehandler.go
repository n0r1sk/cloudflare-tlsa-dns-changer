// Copyright 2020 N0r1sk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"context"
	"fmt"
	"strings"
	"time"

	"github.com/cloudflare/cloudflare-go"
	log "github.com/sirupsen/logrus"
)

func checkRecordFromCloudflareWithGenerated(zone string, record tlsaRecordObj, ca bool) (bool, cloudflare.DNSRecord) {
	// Retrieve existing DNS records from Cloudflare for the given zone and record name
	recs, _, err := api.ListDNSRecords(context.Background(), cloudflare.ZoneIdentifier(zone), cloudflare.ListDNSRecordsParams{Type: "TLSA", Name: record.Name})
	if err != nil {
		log.Fatal(err)
	}

	for _, r := range recs {
		// Parse existing record content
		existingUsage, existingSelector, existingMatchingType, existingCertificate := parseTLSAContent(r.Content)
		newUsage, newSelector, newMatchingType, newCertificate := parseTLSAContent(record.Content)

		// Check if the entire record content matches
		if existingUsage == newUsage && existingSelector == newSelector && existingMatchingType == newMatchingType && existingCertificate == newCertificate {
			// Matching record found with the correct content
			return true, r
		} else if existingUsage == newUsage {
			// Record exists with the same usage but different content, needs an update
			return false, r
		}
	}
	// No matching record found, return false and an empty record object
	return false, cloudflare.DNSRecord{}
}

func parseTLSAContent(content string) (string, string, string, string) {
	// Split the content into its components
	parts := strings.Fields(content)
	if len(parts) != 4 {
		return "", "", "", ""
	}
	return parts[0], parts[1], parts[2], parts[3]
}

func getAllDomainNamesFromCloudflare() map[string][]string {
	zones, err := api.ListZones(context.Background(), serviceConfig.Cloudflare.Domains...)
	if err != nil {
		log.Fatal(err)
	}
	domains := make(map[string][]string)
	for _, zone := range zones {
		processZoneDNSRecords(zone.ID, domains)
	}
	return domains
}

func processZoneDNSRecords(zoneID string, domains map[string][]string) {
	recs, _, err := api.ListDNSRecords(context.Background(), cloudflare.ZoneIdentifier(zoneID), cloudflare.ListDNSRecordsParams{Type: "TLSA"})
	if err != nil {
		log.Fatal(err)
	}

	for _, r := range recs {
		name := strings.TrimPrefix(r.Name, "*._tcp.")
		if !hasElem(domains[zoneID], name) {
			domains[zoneID] = append(domains[zoneID], name)
		}
	}
}

func updateCloudflareRecord(zone string, cfrecord cloudflare.DNSRecord, record tlsaRecordObj, ca bool, d string) {
	// Update the record only if the content does not match
	if !isContentEqual(cfrecord.Content, record.Content) {
		updateRecordContent(&cfrecord, record.Content)
		updateParams := &cloudflare.UpdateDNSRecordParams{
			ID:      cfrecord.ID,
			Name:    cfrecord.Name,
			Content: cfrecord.Content,
			Data:    cfrecord.Data,
			Comment: cloudflare.StringPtr(fmt.Sprintf("Updated by Cloud-TDC on %s", time.Now())),
		}
		updateDNSRecord(zone, *updateParams)
		log.Infof("Updated TLSA %s for %s", formatRecordName(ca, d), d)
	}
}

func isContentEqual(existingContent, newContent string) bool {
	// Check if all parts of the content match
	existingParts := strings.Fields(existingContent)
	newParts := strings.Fields(newContent)
	if len(existingParts) != len(newParts) {
		return false
	}
	for i := range existingParts {
		if existingParts[i] != newParts[i] {
			return false
		}
	}
	return true
}

func updateRecordContent(cfrecord *cloudflare.DNSRecord, newContent string) {
	// Update the DNS record content and certificate data
	cfrecord.Content = newContent
	tlsadata := cfrecord.Data.(map[string]interface{})
	tlsadata["certificate"] = strings.Split(newContent, "\t")[3]
	cfrecord.Data = tlsadata
}

func updateDNSRecord(zone string, record cloudflare.UpdateDNSRecordParams) {
	// Update the DNS record in Cloudflare
	_, err := api.UpdateDNSRecord(context.Background(), &cloudflare.ResourceContainer{Identifier: zone}, record)
	if err != nil {
		log.Errorf("Could not update record with the message: %s", err)
	}
}

func createDNSRecord(zone string, record cloudflare.CreateDNSRecordParams) {
	// Create a new DNS record in Cloudflare
	_, err := api.CreateDNSRecord(context.Background(), &cloudflare.ResourceContainer{Identifier: zone}, record)
	if err != nil {
		log.Errorf("Could not create record with the message: %s", err)
	}
}

func createCloudflareRecord(zone string, record tlsaRecordObj) {
	// Prepare the DNS record creation parameters
	tlsarecord := cloudflare.CreateDNSRecordParams{
		Type:    "TLSA",
		Name:    record.Name,
		Content: record.Content,
		TTL:     1,
		Comment: fmt.Sprintf("Created by Cloud-TDC on %s", time.Now()),
	}
	contentSplit := strings.Split(record.Content, "\t")

	tlsadata := map[string]interface{}{
		"usage":         parseToInt(contentSplit[0]),
		"selector":      parseToInt(contentSplit[1]),
		"matching_type": parseToInt(contentSplit[2]),
		"certificate":   contentSplit[3],
	}
	tlsarecord.Data = tlsadata
	createDNSRecord(zone, tlsarecord)
}
