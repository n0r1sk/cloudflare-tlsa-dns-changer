## [1.2.5](https://gitlab.com/n0r1sk/cloudflare-tlsa-dns-changer/compare/v1.2.4...v1.2.5) (2021-10-04)


### Bug Fixes

* update dockerfile ([503761a](https://gitlab.com/n0r1sk/cloudflare-tlsa-dns-changer/commit/503761a5dbebf75c2f2ae3286abb70adc531f6e7))

## [1.2.4](https://gitlab.com/n0r1sk/cloudflare-tlsa-dns-changer/compare/v1.2.3...v1.2.4) (2021-10-04)


### Bug Fixes

* update dockerfile ([7726338](https://gitlab.com/n0r1sk/cloudflare-tlsa-dns-changer/commit/77263383eaa89e9231b411ed6235666759f4966a))

## [1.2.3](https://gitlab.com/n0r1sk/cloudflare-tlsa-dns-changer/compare/v1.2.2...v1.2.3) (2021-10-04)


### Performance Improvements

* add semantic-release ([455a36b](https://gitlab.com/n0r1sk/cloudflare-tlsa-dns-changer/commit/455a36b07edcc341f302ab9e2d932ed4d34782cb))

## [1.2.2](https://gitlab.com/n0r1sk/cloudflare-tlsa-dns-changer/compare/v1.2.1...v1.2.2) (2021-10-04)


### Bug Fixes

* update dependencies ([81ba30c](https://gitlab.com/n0r1sk/cloudflare-tlsa-dns-changer/commit/81ba30c8941f31588048a3f74ee2d203b38818b8))
