# CloudTDC - Cloudflare TLSA DNS Changer

## What it Does

`CloudTDC` is a service solution to dynamically update TLSA records on Cloudflare using its API. `CloudTDC` focuses on simplicity and is designed as a simple Golang binary Docker image configured through a single configuration file.

## What it Doesn't Do

`CloudTDC` has no restrictions on which hosts are tracked. Initially, it connects to Cloudflare and retrieves all existing TLSA records. These records will be tracked and updated as needed. You can manually add hosts to be tracked.

## Usage

### General Usage

To use `CloudTDC`, edit the `config` file and add your Cloudflare token, domains, and other configuration settings. If the `domains` variable is left empty, all available domains will be used!

### Example `config` Configuration:

```yaml
cloudflare:
  # Domains and Token from the Cloudflare API
  domains: ["example.com", "anotherdomain.com"]
  token: "your_cloudflare_api_token"

tlsacheck:
  # Enable TLSA checks
  use: true
  # Domains to ignore during TLSA checks
  ignoredDomains: ["ignorethisdomain.com", "ignoreanother.com"]
  # Mail servers to check
  mailServers: ["mail.example.com", "mail.anotherdomain.com"]

mailNotification:
  # Enable mail notifications
  use: true
  from: "noreply@example.com"
  password: "your_email_password"
  smtp: "smtp.example.com"
  port: 587
  to: "admin@example.com"
```

### Important

To add a host to be tracked, you must manually add a TLSA record in Cloudflare, which does not need to be valid. The `CloudTDC` service will correct it. The record must start with `*._tcp.` to indicate that it listens to all ports (most commonly `443` or mail ports).

**RAW Example:**

```text
*._tcp.www.example.com.	1	IN	TLSA	3 1 1 8e69d507a12c3d05550c0e0e80ff47a9dd0cd8bd70cb8dcf1841cf7a9c45d26c
```

### New Features

1. **Ignored Domains Configuration**: You can now specify domains to be ignored during TLSA checks using the `ignoredDomains` field in the `config` file. This allows you to exclude certain domains from being checked or updated.

2. **Improved Error Handling and Logging**:
   - The service now provides more accurate error messages when commands fail, including both `stdout` and `stderr` output for better debugging.
   - New logging added to indicate when a domain check is skipped due to being in the ignored list.

3. **Optimized `sendmail.sh` Script**:
   - The `sendmail.sh` script now uses `s-nail`'s built-in `urlcodec` to ensure proper URL encoding for SMTP credentials, reducing errors and improving compatibility.

## Start Service

To start the service, download the [config](https://gitlab.com/n0r1sk/cloudflare-tlsa-dns-changer/raw/master/config) file and the [start_cloud_tdc.sh](https://gitlab.com/n0r1sk/cloudflare-tlsa-dns-changer/raw/master/start_cloud_tdc.sh) script.

Make the script executable:

```bash
$ chmod u+x start_cloud_tdc.sh
```
After filling in the config file, simply run the script, and a Docker container will appear. That’s all!

## Script and Service Optimization

### `sendmail.sh` Script Improvements:
- The script now ensures proper URL encoding for SMTP credentials using `s-nail`'s built-in `urlcodec` functionality. This prevents errors related to special characters in email addresses or passwords and improves compatibility with modern SMTP servers.
- The script also handles exit statuses more accurately, providing clear feedback in case of failures.

### Command Execution Optimization:
- The `executeSingleCommand` function has been optimized to provide better error handling. It now captures both `stdout` and `stderr` outputs when a command fails, allowing for more detailed debugging and error reporting.
- The function returns both the standard output and a comprehensive error message when an error occurs, making troubleshooting more straightforward.

## Summary

`CloudTDC` provides a simple and effective way to manage TLSA records on Cloudflare with a focus on automation, error handling, and flexibility. With the newly added configuration options and optimized error handling, managing TLSA records has never been easier.

Feel free to contribute, report issues, or suggest improvements to make `CloudTDC` even better!