// Copyright 2020 N0r1sk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import (
	"time"

	cloudflare "github.com/cloudflare/cloudflare-go"
	log "github.com/sirupsen/logrus"
)

var (
	lastEmailTime  time.Time
	emailSentCount int
)

func main() {
	// Load configuration
	serviceConfig = &serviceConfigObj{configFilename: "/etc/dns-changer/dns-changer.yaml"}
	loadConfig()

	// Set up logging
	setupLogging()

	// Add custom hosts
	addConfigHosts()

	// Initialize Cloudflare API
	initializeCloudflareAPI()

	// Fetch domain names and start DNS checks
	domainsWithZones = getAllDomainNamesFromCloudflare()
	checkDNSRecords()
}

func initializeCloudflareAPI() {
	var err error
	api, err = cloudflare.NewWithAPIToken(serviceConfig.Cloudflare.Token)
	if err != nil {
		log.Panicf("Cannot open connection to Cloudflare: %s", err)
	}
}

func checkDNSRecords() {
	for {
		errors, failedDomains, needsSend := performDNSChecks()

		// Rate limit email notifications to twice per day
		if needsSend && serviceConfig.MailNotification.Use && canSendEmail() {
			sendEmail(errors, failedDomains)
			emailSentCount++
			lastEmailTime = time.Now()
		}

		// Sleep and refresh domain names periodically
		time.Sleep(time.Minute * time.Duration(serviceConfig.TLSAUpdate.CheckRecordsTimer))
		domainsWithZones = getAllDomainNamesFromCloudflare()
	}
}
