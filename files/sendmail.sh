#!/bin/bash

# Ensure all arguments are provided
if [ "$#" -ne 7 ]; then
  echo "Usage: $0 <body> <from_email> <domain> <smtp_server> <smtp_port> <smtp_password> <to_email>"
  exit 1
fi

# Define variables for clarity
BODY="$1"
FROM_EMAIL="$2"
DOMAIN="$3"
SMTP_SERVER="$4"
SMTP_PORT="$5"
SMTP_PASSWORD="$6"
TO_EMAIL="$7"

# Use the modern parameters for s-nail
echo "$BODY" | s-nail -v \
    -r "$FROM_EMAIL" \
    -s "Cloud-TDC: DANE TLSA configuration issue for $DOMAIN" \
    -S mta="smtp://$SMTP_SERVER:$SMTP_PORT" \
    -S smtp-use-starttls \
    -S smtp-auth=login \
    -S smtp-auth-user="$FROM_EMAIL" \
    -S smtp-auth-password="$SMTP_PASSWORD" \
    "$TO_EMAIL"
