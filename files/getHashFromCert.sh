# Copyright 2020 N0r1sk
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

mkdir -p $2
rm -rf $2/{ca-$1,$1}.pem
name=$1
if [ $3 == "true" ]
then 
    name=$(echo "$1" | awk -F '.' '{print $(NF-1) "." $NF}')
fi
ssl=$(timeout 15 openssl s_client -showcerts -verify 5 -connect $name:443 < /dev/null)
if [ $? != 0 ]
then
    exit 1
fi
echo "$ssl" | awk '/BEGIN/,/END/{ if(/BEGIN/){a++}; out="cert"a".pem"; print >out}'

mv cert1.pem $2/$1.pem
mv cert2.pem $2/ca-$1.pem
rm -rf cert*.pem
