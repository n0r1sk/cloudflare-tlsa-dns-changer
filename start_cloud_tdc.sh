#!/bin/bash
# Copyright 2020 N0r1sk
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#      http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

docker run --net=host --restart=always --name=cloud_tdc --env-file dns-env -d \
  -v dns-changer.yaml:/etc/dns-changer/dns-changer.yaml \
  ringeltier/cloudflare-tlsa-dns-changer:v1.4.5
