// Copyright 2020 N0r1sk
//
// Licensed under the Apache License, Version 2.0 (the "License");
// you may not use this file except in compliance with the License.
// You may obtain a copy of the License at
//
//      http://www.apache.org/licenses/LICENSE-2.0
//
// Unless required by applicable law or agreed to in writing, software
// distributed under the License is distributed on an "AS IS" BASIS,
// WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
// See the License for the specific language governing permissions and
// limitations under the License.

package main

import cloudflare "github.com/cloudflare/cloudflare-go"

var (
	api              *cloudflare.API
	serviceConfig    *serviceConfigObj
	domainsWithZones map[string][]string
)

type serviceConfigObj struct {
	configFilename      string
	OutputCerts         string
	UseAlwaysRootDomain bool
	HostEntries         []hostEntry
	Cloudflare          struct {
		Token   string
		Domains []string
	}
	TLSAUpdate struct {
		Use               bool
		CheckRecordsTimer int
	}
	TLSACheck struct {
		Use            bool
		MailServers    []string
		IgnoredDomains []string
	}
	MailNotification struct {
		Use      bool
		From     string
		Password string
		SMTP     string
		Port     int
		To       string
	}
}

type hostEntry struct {
	FQDN string
	IP   string
}

type tlsaRecordObj struct {
	Name    string
	Content string
}
