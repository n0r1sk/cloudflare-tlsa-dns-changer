package main

import (
	"fmt"
	"strconv"
	"strings"

	log "github.com/sirupsen/logrus"
)

func getAndSaveCertificates(domainname string) bool {
	out, err := executeSingleCommand("/bin/bash", "/etc/dns-changer/getHashFromCert.sh", domainname, serviceConfig.OutputCerts, strconv.FormatBool(serviceConfig.UseAlwaysRootDomain))
	if err != nil {
		if !strings.Contains(err.Error(), "DONE") {
			logErrorBasedOnMessage(err.Error(), domainname)
			return false
		}
	}
	if out != "" {
		fmt.Println(out)
	}
	log.Infof("The domain %s will be checked", domainname)
	return true
}

func logErrorBasedOnMessage(errorMsg, domainname string) {
	switch {
	case strings.Contains(errorMsg, "No address associated"):
		log.Errorf("No route exists to this record %s. Skip", domainname)
	case strings.Contains(errorMsg, "exit status 1"):
		log.Errorf("Could not reach the IP address behind this record %s. Skip", domainname)
	default:
		log.Errorf("Cannot get certificate with the error: %s", errorMsg)
	}
}

func getCertificatePath(domainname string, ca bool) string {
	filename := domainname + ".pem"
	if ca {
		filename = "ca-" + filename
	}
	return fmt.Sprintf("%s/%s", serviceConfig.OutputCerts, filename)
}

func createTLSARecord(domainname string, ca bool) tlsaRecordObj {
	args := []string{"--tlsa-rr", "--load-certificate", getCertificatePath(domainname, ca), "--host", domainname}
	if ca {
		args = append(args, "--ca")
	}

	out, err := executeSingleCommand("danetool", args...)
	if err != nil {
		sendEmail(err.Error(), fmt.Sprintf("Critical error on DANE TLSA record creation for %s. Check immediately!", domainname))
		log.Panic(out, err.Error())
	}

	name, content := parseTLSAOutput(out)
	return tlsaRecordObj{Name: name, Content: content}
}

func parseTLSAOutput(out string) (string, string) {
	name := strings.Replace(strings.Split(out, " ")[0], "_443", "*", 1)
	start, end := strings.Index(out, "("), strings.Index(out, ")")
	tlsaParts := strings.Fields(out[start+1 : end])
	return strings.TrimSuffix(name, "."), fmt.Sprintf("%s\t%s\t%s\t%s", trimLeadingZeros(tlsaParts[0]), trimLeadingZeros(tlsaParts[1]), trimLeadingZeros(tlsaParts[2]), tlsaParts[3])
}

func trimLeadingZeros(value string) string {
	return strings.TrimLeft(value, "0")
}

func checkSpecificRecord(zone, d string, ca bool) {
	record := createTLSARecord(d, ca)
	existing, cfrecord := checkRecordFromCloudflareWithGenerated(zone, record, ca)
	if !existing && serviceConfig.TLSAUpdate.Use {
		if cfrecord.ID == "" {
			createCloudflareRecord(zone, record)
			log.Infof("Created TLSA Record for %s", formatRecordName(ca, d))
		} else {
			updateCloudflareRecord(zone, cfrecord, record, ca, d)
		}
	}
}

func isDomainIgnored(domain string) bool {
	for _, ignored := range serviceConfig.TLSACheck.IgnoredDomains {
		if domain == ignored {
			return true
		}
	}
	return false
}

func formatRecordName(ca bool, domain string) string {
	if ca {
		return "ca-" + domain
	}
	return domain
}

func verifyRecord(domainname string) (bool, string) {
	log.Infof("Verifying Domain %s", domainname)
	out, err := executeSingleCommand("danetool", "--check", domainname, "--proto", "tcp", "--port", "443", "--load-certificate", getCertificatePath(domainname, false))
	if err != nil {
		// Return true on failure
		return true, fmt.Sprintf("Out: %s\n\nError: %s", out, err.Error())
	}
	// Return false on success
	return false, out
}

func updateDomainCertificates(zone string, domains []string) {
	for _, d := range domains {
		if !getAndSaveCertificates(d) {
			domainsWithZones[zone] = stringSliceRemoveValue(domainsWithZones[zone], d)
		}
	}
}

func performDNSChecks() (string, string, bool) {
	needsSend := false
	failedDomains := ""
	errors := "The following checks failed:\n"

	// Process domains for certificate checks
	for zone, domains := range domainsWithZones {
		updateDomainCertificates(zone, domains)
	}

	// Check TLSA Records for all domains
	for zone, domains := range domainsWithZones {
		for _, d := range domains {
			// Skip domain if it is in the ignored list
			if isDomainIgnored(d) {
				log.Infof("Skipping TLSA Record check for ignored domain %s", d)
				continue
			}

			log.Infof("Check TLSA Record for %s", d)
			checkSpecificRecord(zone, d, false)
			checkSpecificRecord(zone, d, true)
			if serviceConfig.TLSACheck.Use {
				failed, out := verifyRecord(d) // Updated to use the helper function
				if failed {                    // Adjusted logic to check for failure
					errors += fmt.Sprintf("%s\n", out)
					failedDomains += fmt.Sprintf("%s ", d)
					needsSend = true
				}
			}
		}
	}

	// Check Mail Server Records if configured
	if serviceConfig.TLSACheck.Use {
		for _, domain := range serviceConfig.TLSACheck.MailServers {
			// Skip domain if it is in the ignored list
			if isDomainIgnored(domain) {
				log.Infof("Skipping Mailserver check for ignored domain %s", domain)
				continue
			}

			failed, out := verifyRecord(domain) // Updated to use the helper function
			if failed {                         // Adjusted logic to check for failure
				errors += fmt.Sprintf("%s\n", out)
				failedDomains += fmt.Sprintf("%s ", domain)
				needsSend = true
			}
		}
	}

	return errors, failedDomains, needsSend
}
